#!/bin/bash

echo 'CIRCLE_BRANCH: ' ${CIRCLE_BRANCH}

files=$(git diff --name-only ${CIRCLE_BRANCH} origin/master | grep '.js')

error=false
for file in ${files}; do
  echo 'file:' $file
  result=$(./node_modules/.bin/eslint ${file} | grep 'error')
  if [ "$result" != '' ]; then
    error=true
    echo 'error:' $file
    echo $result
  fi
done

if $error; then
  exit 1
fi

exit 0
